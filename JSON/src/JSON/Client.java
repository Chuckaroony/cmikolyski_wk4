package JSON;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.*;
import java.io.*;
import java.nio.Buffer;
import java.util.*;

public class Client {

    // This method maps JSON to a user object.
    public static User JSONToUser(String string) {

        ObjectMapper mapper = new ObjectMapper();
        User user = null;

        try {
            user = mapper.readValue(string, User.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString() + "The data coming from the server is incorrect or non existent.");
        }

        return user;
    }
    // This is the class used to connect to the HTTP server, read the inputstream,
    // and turn it into a JSON string.
    public static String getHttpContent(String string)throws MalformedURLException, IOException  {

        String content="";

        try {
            // This creates the url with the passed string parameter from the method call.
            URL url = new URL(string);

            // This creates the http connection call.
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            //This reads the data served by the server using the input stream.
            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));

            //This builds a string from the input string.
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            content = stringBuilder.toString();


        } catch (MalformedURLException me) {
        System.err.println(me+ " There is a problem with the connection to the server. The url maybe incorrect.");
    } catch (IOException io) {
            System.err.println(io + " There is a problem with the connection to the server.");
        } catch (Exception e) {
            System.err.println(e.toString()+ " There is a problem reading the data from the server.");
        }
        return content;
    }

    //Main method for running the client alone for testing purposes.
    public static void main(String[] args) throws IOException {

        // User user1 = Client.JSONToUser(Client.getHttpContent("http://localhost:8000"));
        //Display the user object to the screan.
        //System.out.println(user1);
    }
}

