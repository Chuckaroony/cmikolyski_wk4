package JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.net.*;
import java.io.*;
import java.nio.Buffer;
import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;


//This com.sun.net.httpserver import is the API that provides the tools
//to create a server program that listens on a port and url.
import com.sun.net.httpserver.*;




public class Server {

    //This method maps the User object to JSON.
    public static String userToJSON (User user){

        ObjectMapper mapper = new ObjectMapper();
        String mapToJSON = "";

        try {
            mapToJSON = mapper.writeValueAsString(user);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString()+ " There is no object to be converted to JSON.");
        }

        return mapToJSON;
    }
    //This is the method that creates the server, gives it a socket address and port.
    //the url and port are passed as parameters to the method when called in JSON main class.
    public static HttpServer createServer(String url, int port) throws IOException {
        HttpServer server =null;
        try{
            //This creates the instance of the server and assigns a url and a port number.
            // It also adds a backlog of the connection.
            server = HttpServer.create(new InetSocketAddress(url, port), 0);

            //This sets the context or, root file location, of the server, which it does not have.
            HttpContext context = server.createContext("/");

            //This line sets the context for the server to be handled by the handleRequest method.
            context.setHandler(Server::handleRequest);
        } catch (IOException io) {
            System.err.println(io.toString()+ " There is a problem with the url address or port number.");
        }

        //This returns the created server to be used on the main JSON class.
        return server;
    }

        //The handleRequest method handles the exchange between the the server and the client
        // and provides the JSON to send to the client. It creates the user object and
        // populates the data for the object.
        private static void handleRequest(HttpExchange exchange) throws IOException {

        // This creates the user object.
        User user1 = new User();
        user1.setAcctID(1);
        user1.setName("Chuck");

        //This calls the method that converts the user object to a JSON string
            // and stores the returned value in the response variable.
        String response = userToJSON(user1);

        //This group of commands sends the response and headers to the requesting client.
        exchange.sendResponseHeaders(200, response.getBytes().length);
        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());

        //this closes the outputstream after the exchange is complete.
        os.close();
        }

    //Main method for running server alone for testing purposes.
    public static void main(String[] args) throws IOException {

        //HttpServer server = createServer("localhost", 8000); {
        //server.start();
        //}
    }


}

