package JSON;

public class User {
    String name;
    Integer acctID;


    public String toString() {
        return "User: " + name + " ID: " + acctID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAcctID() {
        return acctID;
    }

    public void setAcctID(Integer acctID) {
        this.acctID = acctID;
    }
}
