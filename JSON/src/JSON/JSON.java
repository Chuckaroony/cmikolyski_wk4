package JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.net.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.Executor;

import com.sun.net.httpserver.*;

public class JSON {

    
    public static void main(String[] args) throws IOException {
        //This creates the new instance of the server.
        HttpServer server = Server.createServer("localhost",8000);

        //This starts the server and indicates it started.
        try{
            server.start();
            System.out.println("Server is running...");
        } catch (NullPointerException e) {
            System.err.println("Cannot start server because it does not exist.");
        }


        //This creates the user object and fills it with the user object created after
        //receiving the JSON from the server call.
        User user1 = Client.JSONToUser(Client.getHttpContent("http://localhost:8000"));
        //Display the user object to the screen.
        System.out.println(user1);

        //This shuts down the server and notifies the user that it stopped.
        server.stop(0);
        System.out.println("Server stopped.");

    }

}


